Zoviz is an unaffiliated library for accessing and visualizing Zotero data. 

Gitlab project: https://gitlab.com/jlogan03/zoviz (includes example.ipynb notebook)

RTFM: https://zoviz.readthedocs.io/en/latest/index.html

Example application: visualizing the graph of collaboration between creators

![creator graph]( ./ci/docs/source/community_graph.png "Community graph" )
