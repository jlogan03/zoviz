"""Package metadata"""

__version__ = "0.4.{ci_build_rev}"
__author__ = "James Logan"
__author_email__ = "jlogan03@gmail.com"
__description__ = "Unaffiliated visualization helper for Zotero data"
__url__ = "https://gitlab.com/jlogan03/zoviz"
